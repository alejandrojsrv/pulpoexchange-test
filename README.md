# Pulpo Exchange

Pulpo Exchange es una aplicación Symfony que permite a los usuarios hacer conversiones entre diferentes monedas y registra estas solicitudes en la API.

## Planteamiento de la solución

Para obtener las tasas de cambio, hago una solicitud a la API de CurrencyLayer desde el backend de Symfony. Elegí hacerlo de esta manera porque la API de CurrencyLayer requiere una clave API para autenticarse, y exponer esta clave API en el frontend sería inseguro. Al hacer las solicitudes a la API de CurrencyLayer desde el backend, puedo mantener mi clave API segura y evitar que sea expuesta en el cliente.

Además de obtener las tasas de cambio, también registro todas las solicitudes a mi API. Para hacer esto, creo una entidad `ApiRequestLog` que registra el endpoint, la fecha y hora de la solicitud, y la dirección IP del cliente. Cada vez que se recibe una solicitud, creo una nueva instancia de `ApiRequestLog` y la guardo en la base de datos.

Para limitar las solicitudes, utilizo el componente RateLimiter de Symfony para limitar el número de solicitudes que un cliente puede hacer en un período de tiempo determinado.

La interfaz de usuario se construye con Vue.js.

## Versiones

- PHP: 8.1.25
- MySQL: 5.7
- Vue.js: 3.2.36
- Symfony: 6.1.11

## Instalación

1. Clona este repositorio en tu máquina local con `git clone`.

2. Navega hasta el directorio del proyecto con `cd pulpo-exchange`.

3. Instala las dependencias del proyecto con `composer install`.

4. Instala las dependencias de Vue.js con `npm install`.

5. Crea la base de datos con `php bin/console doctrine:database:create`.

6. Ejecuta las migraciones de la base de datos con `php bin/console doctrine:migrations:migrate`.

7. Compila tus archivos Vue.js con `npm run build`.

8. Inicia el servidor de desarrollo con `symfony server:start`.

## Uso

Puedes hacer solicitudes a la API en el endpoint `/api/rates` enviandole el parametro `source` con el código de la moneda. Esto devolverá las tasas de cambio actuales para esa moneda en formato JSON.

Para realizar una conversión de monedas puedes visitar la ruta `/rates`. Esto mostrará un formulario en el que podras realizar la conversión.

Para ver las solicitudes registradas en tu navegador, puedes visitar la ruta `/requests`. Esto mostrará una tabla con todas las solicitudes registradas de otros clientes.

## Autor
Alejandro Sojo