/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.css';

// start the Stimulus application
import './bootstrap';
// assets/app.js
// assets/app.js
import { createApp } from 'vue';
import Navbar from './components/Navbar.vue';
import CurrencyConverter from './components/CurrencyConvert.vue';
import RequestList from './components/RequestList.vue';

import vSelect from 'vue-select';

const app = createApp({});

app.component('navbar', Navbar);
app.component('currency-converter', CurrencyConverter);
app.component('request-list', RequestList);
app.component('v-select', vSelect);

app.mount('#app');