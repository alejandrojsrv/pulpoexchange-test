<?php

// src/Service/ApiService.php
namespace App\Service;

use Symfony\Contracts\HttpClient\HttpClientInterface;

class ApiService
{
    private $client;
    private $apiKey;

    private $baseURL = "http://api.currencylayer.com";

    public function __construct(HttpClientInterface $client, string $apiKey)
    {
        $this->client = $client;
        $this->apiKey = $apiKey;
    }

    public function getRateLives(string $source)
    {
        $response = $this->client->request(
            'GET',
            "{$this->baseURL}/live",
            [
                'query' => [
                    "access_key" => $this->apiKey,
                    'source' => $source
                ],
            ]
        );

        return $response->toArray();
    }

    public function getListCurrencyAvailable()
    {
        $response = $this->client->request(
            'GET',
            "{$this->baseURL}/list",
            [
                'query' => [
                    "access_key" => $this->apiKey
                ],
            ]
        );

        return $response->toArray();
    }


    public function convertCurrency(
        string $from,
        string $to,
        float $amount
    ) {

        $response = $this->client->request(
            'GET',
            "{$this->baseURL}/convert",
            [
                'query' => [
                    "access_key" => $this->apiKey,
                    "from" => $from,
                    "to" => $to,
                    "amount" => $amount,
                ],
            ]
        );

        return $response->toArray();
    }

    // Add more methods for other endpoints...
}