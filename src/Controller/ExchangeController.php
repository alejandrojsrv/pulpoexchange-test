<?php

namespace App\Controller;

use App\Service\ApiService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ExchangeController extends AbstractController
{

    private $apiService;

    public function __construct(ApiService $apiService)
    {
        $this->apiService = $apiService;
    }


    #[Route('/', name: 'app_exchange_index')]
    public function index(Request $request)
    {

        $formCurrency = [
            'from' => $request->get('from','USD'),
            'to' => $request->get('to', 'EUR'),
            'amount' => $request->get('amount', '1000')
        ];

        $currencyAvailable = $this->apiService->getListCurrencyAvailable();

        return $this->render('exchange/index.html.twig', [
            'controller_name' => 'ExchangeController',
            'currencies' => $currencyAvailable['currencies'],
            'formCurrency' => $formCurrency
        ]);
    }


    #[Route('/limit-reached', name: 'app_exchange_limit_exceded')]
    public function limit_reached(): Response
    {
        return $this->render('limit-reached.html.twig');
    }
}
