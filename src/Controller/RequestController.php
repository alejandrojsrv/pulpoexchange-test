<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\ApiRequestLog;
use App\Repository\ApiRequestLogRepository;

class RequestController extends AbstractController
{
    #[Route('/requests', name: 'app_requests')]
    public function getRequests(ApiRequestLogRepository $repository): Response
    {
        $requests = $repository->findAll();
        $requestsList = [];

        foreach ($requests as $request) {
            $requestsList[] = [
                "id" => $request->getId(),
                "ip" => $request->getIp(),
                "endpoint" => $request->getEndpoint(),
                "requestedAt" => $request->getRequestedAt()->getTimestamp(),
            ];
        }



        return $this->render('request.html.twig', ['requests' => $requestsList]);
    }
}