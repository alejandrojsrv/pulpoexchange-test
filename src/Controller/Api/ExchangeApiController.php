<?php

namespace App\Controller\Api;

use App\Entity\ApiRequestLog;
use App\Service\ApiService;
use App\Service\ConversionRateLimiterService;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\TooManyRequestsHttpException;
use Symfony\Component\RateLimiter\RateLimiterFactory;
use Symfony\Component\Routing\Annotation\Route;

class ExchangeApiController extends AbstractController
{
    private $apiService;

    public function __construct(ApiService $apiService)
    {
        $this->apiService = $apiService;
    }


    #[Route('/api/convert', name: 'api_exchange_convert')]
    public function convertAmount(Request $request, RateLimiterFactory $anonymousApiLimiter): JsonResponse
    {

        try {

            $limiter = $anonymousApiLimiter->create($request->getClientIp());

            $limit = $limiter->consume();

            if (!$limit->isAccepted()) {
                throw new Exception("Has alcanzado el limite de consultas por hoy", 429);
            }

            $from = $request->get('from');
            $to = $request->get('to');
            $amount = $request->get('amount');

            $result = $this->apiService->convertCurrency($from, $to, $amount);

            return $this->json($result, Response::HTTP_OK);

        } catch (Exception $e) {
            return $this->json(['message' => $e->getMessage()], $e->getCode());
        }
    }


    #[Route('/api/rates', name: 'api_exchange_rates')]
    public function getRates(Request $request, RateLimiterFactory $anonymousApiLimiter, EntityManagerInterface $entityManager): JsonResponse
    {

        try {

            // Registra la solicitud
            $log = new ApiRequestLog();
            $log->setEndpoint($request->getRequestUri());
            $log->setRequestedAt(new \DateTimeImmutable());
            $log->setIp($request->getClientIp());

            $entityManager->persist($log);
            $entityManager->flush();

            $limiter = $anonymousApiLimiter->create($request->getClientIp());

            $limit = $limiter->consume();

            if (!$limit->isAccepted()) {
                throw new Exception("Has alcanzado el limite de consultas por hoy", 429);
            }

            $source = $request->get('source');

            $result = $this->apiService->getRateLives($source);

            return $this->json($result, Response::HTTP_OK);

        } catch (Exception $e) {
            return $this->json(['message' => $e->getMessage()], $e->getCode() != 0 ? $e->getCode() : 422);
        }
    }
}
