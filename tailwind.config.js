module.exports = {
  content: [
    './templates/**/*.twig',
    './assets/**/*.vue',
    './assets/**/*.js',
    // etc.
  ],
  theme: {
    extend: {},
  },
  plugins: [],
}